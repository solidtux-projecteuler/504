fn mf(a: usize, b: usize) -> u64 {
    let af = a as f64;
    let bf = b as f64;
    (0..b)
        .map(|x| (af * (bf - x as f64) / bf).ceil() as u64)
        .sum()
}

fn issqrt(x: u64) -> bool {
    ((x as f64).sqrt() as u64).pow(2) == x
}

fn f(x: usize) -> u64 {
    let mut res = 0;
    let mut m = vec![vec![0; x]; x];
    for a in 0..x {
        for b in 0..x {
            m[a][b] = mf(a + 1, b + 1);
        }
    }
    for a in 0..x {
        for b in 0..x {
            for c in 0..x {
                for d in 0..x {
                    if issqrt(
                        m[a][b] + m[b][c] + m[c][d] + m[d][a] - (a as u64) - (b as u64) - (c as u64)
                            - (d as u64) - 4 + 1,
                    ) {
                        res += 1;
                    }
                }
            }
        }
    }
    res
}

fn main() {
    println!("{}", f(100));
}
